<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::prefix('api')->group(function(){
    Route::get('newsposts','NewsPostController@index');
    Route::get('newspost/{id}','NewsPostController@show');
    Route::get('newspostcat/{id}','NewsPostController@getCategory');
    Route::get('categories','CategoryController@index');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::prefix('api')->middleware('auth')->group(function() {

    Route::post('newspost','NewsPostController@store');
    Route::put('newspost','NewsPostController@store');
    Route::delete('newspost/{id}','NewsPostController@destroy');

    Route::get('category/{id}','CategoryController@show');
    Route::post('category','CategoryController@store');
    Route::put('category','CategoryController@store');
    Route::delete('category/{id}','CategoryController@destroy');

    Route::get('users','UserController@index');
    Route::get('user/{id}','UserController@show');
    Route::post('user','UserController@store');
    Route::put('user','UserController@store');
    Route::delete('user/{id}','UserController@destroy');

    Route::post('user/category/new','UserCategoryRelationController@store');
    Route::put('user/category/update','UserCategoryRelationController@store');
    Route::delete('user/category/delete','UserCategoryRelationController@destroy');

Route::group(['prefix'=>'auth','namespace'=>'Auth'], function (){
    Route::post('signin', 'SignInController');
    Route::post('signout', 'SignOutController');
    Route::get('me', 'MeController');
});

//Route::post('login','UserController@login');
//Route::post('register', 'UserController@register');
//Route::group(['middleware' => 'auth:api'], function(){
//    Route::post('details', 'UserController@details');
//});






