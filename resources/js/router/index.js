import VueRouter from "vue-router";
import SignIn from "../components/SignIn";
import NewsPost from "../components/NewsPost";
import Admin from "../components/admin";
import Categories from "../components/Categories"
import SignUp from "../components/SignUp";
import Vue from "vue";
import store from "../store/index"

Vue.use(VueRouter)

const routes = [
    {
        path: '/signin',
        name: 'SignIn',
        component: SignIn,
    },
    {
        path: '/admin',
        name: 'Admin',
        component: Admin,
    },
    {
        path: '/',
        name: 'NewsPost',
        component: NewsPost,
    },
    {
        path: '/categories',
        name: 'Categories',
        component: Categories,
        beforeEnter:(to,from,next)=>{
            if(!store.getters['auth/authenticated']){
                return next({
                    name:'SignIn'
                })
            }
        }
    },
    {
        path: '/admin/signin',
        name: 'SignInAdmin',
        component: Admin,
        beforeEnter:(to,from,next)=>{
            if(!store.getters['auth/authenticated']){
                return next({
                    name:'SignIn'
                })
            }
        }
    },
    {
        path: '/signup',
        name: 'SignUp',
        component: SignUp,
    },
];

const router=new VueRouter({
    mode:'history',
    base:process.env.BASE_URL,
    routes
})

export default router
