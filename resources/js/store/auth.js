import axios from "axios";

export default {
    namespaced: true,
    state: {
        token: null,
        user: null,
    },
    getters:{
        authenticated(state){
            return state.token
        },
    },
    mutations: {
        SET_TOKEN(state,token){
            state.token=token
        },
    },
    actions: {
        async signin({dispatch}, credentials) {
            let response = await axios.post('http://127.0.0.1:8001/api/auth/signin', credentials)

            return dispatch('attempt', response.data.token)
        },
        async attempt({commit,state}, token) {
            if(token){
                commit('SET_TOKEN', token)
            }

            if(!state.token){
                return
            }
        },
        signOut({commit}){
            return axios.post('http://127.0.0.1:8001/api/auth/signout').then(()=>{
                commit('SET_TOKEN', null)
            })
        }
    }
}

