<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NewsPost extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return[
            'id'=>$this->id,
            'category'=>$this->categories_id,
            'title'=>$this->title,
            'url'=>url($this->url),
            'image'=>$this->image,
            'snippets'=>$this->snippets,
        ];
    }

//    public function with($request)
//    {
//        return [
//            'version'=>'1.0.0',
//        ];
//    }
}
