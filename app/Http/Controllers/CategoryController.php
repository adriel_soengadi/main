<?php

namespace App\Http\Controllers;

use App\category;
use App\category_user;
use App\Http\Resources\Category as CategoryResource;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $category=category::all();
        return CategoryResource::collection($category);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return CategoryResource
     */
    public function store(Request $request)
    {
        $category=$request->isMethod('put')? category::findorFail($request->category_id): new category;
        $category_user=$request->isMethod('put')?category_user::findorFail($request->category_id):new category_user;
        $id=$request->input('category_id');
        $name=$request->input('category_name');
        $category->id=$id;
        $category->category_name=$name;
        $category_user->id=$id;
        $category_user->category_name=$name;

        if($category->save() & $category_user->save())
        {
            return new CategoryResource($category);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return CategoryResource
     */
    public function show($id)
    {
        $category=category::findorFail($id);

        return new CategoryResource($category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return CategoryResource
     */
    public function destroy($id)
    {
        $category=category::findorFail($id);
        $category_user=category_user::findorFail($id);

        if($category->delete() & $category_user->delete())
        {
            return new CategoryResource($category);
        }
    }
}
