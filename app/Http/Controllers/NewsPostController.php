<?php

namespace App\Http\Controllers;

use App\category;
use App\news_post;
use App\Http\Resources\NewsPost as NewsPostsResource;
use Illuminate\Http\Request;

class NewsPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $news_posts=news_post::orderBy('created_at', 'desc')->paginate(15);
        return NewsPostsResource::collection($news_posts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return NewsPostsResource
     */
    public function store(Request $request)
    {
        $news_post=$request->isMethod('put')? news_post::findorFail($request->newspost_id): new news_post;
        $category=category::findorFail($request->input('category_id'));
        $news_post->id=$request->input('newspost_id');
        $news_post->category()->associate($category);
        $news_post->title=$request->input('title');
        $news_post->image=$request->input('image');
        $news_post->url=$request->input('url');
        $news_post->snippets=$request->input('snippets');

        if($news_post->save())
        {
            return new NewsPostsResource($news_post);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return NewsPostsResource
     */
    public function show($id)
    {
        $news_post=news_post::findorFail($id);

        return new NewsPostsResource($news_post);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return NewsPostsResource
     */
    public function destroy($id)
    {
        $news_post=news_post::findorFail($id);

        if($news_post->delete())
        {
            return new NewsPostsResource($news_post);
        }
    }

    public function getCategory($id)
    {
        $news_post=news_post::findorFail($id);
        return $news_post->category->category_name;
    }
}
