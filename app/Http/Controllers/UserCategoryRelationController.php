<?php

namespace App\Http\Controllers;

use App\category;
use App\User;
use App\category_user;
use App\Http\Resources\UserCategoryRelation as UserCategoryRelationResource;
use App\user_category_relation;
use Illuminate\Http\Request;

class UserCategoryRelationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return UserCategoryRelationResource
     */
    public function store(Request $request)
    {
        //ke table user_category_relation, yg joint table buat cari user_id pengguna kalo put request
        $categoryuser=$request->isMethod('put')? user_category_relation::findorFail($request->user_id): new user_category_relation;
        //ke table category_user buat cari apakah category_id input user ada di category_user_table
        $user=User::findorFail($request->input('user_id'));

        $category=category_user::findorFail($request->input('category_user_id'));
        //ke table user cari usernya punya id g
        //data dr input utk user_id di link ke table user dengan relasi many to many ke category_user_table
//        $category->user()->attach($user);
        //data dr input utk category_user_id di link ke table user dengan relasi many to many ke user_table
        $user->category_user()->attach($category);

        if($categoryuser->save())
        {
            return new UserCategoryRelationResource($categoryuser);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return UserCategoryRelationResource
     */
    public function show($id)
    {
        $user=category_user::findorFail($id);

        return new UserCategoryRelationResource($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return UserCategoryRelationResource
     */
    public function destroy(Request $request, $user_id)
    {
        $usercategory=user_category_relation::all();
        $user=User::findorFail($user_id);
        $category=category_user::findorFail($request->input('category_user_id'));
        $user->category_user()->attach($category);

        return new UserCategoryRelationResource($usercategory);
    }
}
