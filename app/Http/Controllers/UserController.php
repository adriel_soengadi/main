<?php

namespace App\Http\Controllers;

use App\User;
use App\category_user;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserCategory as UserCategoryResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */

    public $successStatus=200;

    public function login()
    {
        if(Auth::attempt(['email'=>request('email'),'password'=>\request('password')])){
            $user=Auth::user();
            $success['token']=$user->createToken('MyApp')->accessToken;
            return response()->json(['success'=>$success],$this->successStatus);
        }else{
            return response()->json(['error'=>'Unauthorised'],401);
        }
    }

    public function register(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'username'=>'required',
            'email'=>'required|email',
            'password'=>'required',
            'c_password'=>'required|same:password',
        ]);

        if($validator->fails())
        {
            return response()->json(['error'=>$validator->errors()],401);
        }

        $input=$request->all();
        $input['password']=bcrypt($input['password']);
        $user=User::create($input);
        $success['token']=$user->createToken('MyApp')->accessToken;
        $success['name']=$user->name;

        return response()->json(['success'=>$success],$this->successStatus);
    }

    public function details()
    {
        $user=Auth::user();
        return response()->json(['success'=> $user], $this->successStatus);
    }


    public function index()
    {
        $user=User::with('category_user')->paginate(15);
        return UserCategoryResource::collection($user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return UserResource
     */
    public function store(Request $request)
    {
        $User=$request->isMethod('put')? User::findorFail($request->user_id): new User;
        $User->id=$request->input('user_id');
        $User->username=$request->input('username');
        $User->email=$request->input('email');
        $password=Hash::make($request->input('password'));
        $User->password=$password;
        if($User->save())
        {
            return new UserResource($User);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return UserResource
     */
    public function show($id)
    {
        $user=User::findorFail($id);

        return new UserResource($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return UserResource
     */
    public function destroy($id)
    {
        $user=User::findorFail($id);

        if($user->delete())
        {
            return new UserResource($user);
        }

    }
}
