<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SignInAdminController extends Controller
{
    public function __invoke(Request $request)
    {
        $credentials=request(['email','password']);
        if(!$token=auth()->attempt($credentials)){
            return response(null,401);
        }
        return response()->json(compact('token'));
    }
}
