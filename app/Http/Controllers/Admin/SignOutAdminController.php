<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SignOutAdminController extends Controller
{
    public function __invoke()
    {
        auth()->logout();
    }
}
