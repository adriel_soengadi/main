<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SignInController extends Controller
{
    public function __invoke(Request $request)
    {
        $credentials=request(['email','password']);
        if(!$token=auth()->attempt($credentials)){
            return response(null,401);
        }
        return response()->json(compact('token'));
    }
}
