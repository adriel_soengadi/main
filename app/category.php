<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected $connection='mysql2';
    protected $table='categories';

    public function newspost()
    {
        return $this->hasMany(news_post::class);
    }
}
