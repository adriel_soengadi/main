<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category_user extends Model
{
    protected $table='category_users';

    public function user()
    {
        return $this->belongsToMany(User::class,'user_category_relations');
    }
}
