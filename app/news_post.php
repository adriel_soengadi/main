<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class news_post extends Model
{
    protected $connection='mysql2';
    protected $table='news_posts';

    public function category()
    {
        return $this->belongsTo(category::class);
    }
}
