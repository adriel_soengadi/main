<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserCategoryRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_category_relations', function (Blueprint $table) {
            $table->unsignedBigInteger('category_user_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
            $table->foreign('category_user_id')->references('id')->on('category_users');
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_category_relations');
    }
}
