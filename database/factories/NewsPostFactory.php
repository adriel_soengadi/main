<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\news_post::class, function (Faker $faker) {
    return [
        'title'=>$faker->text(50),
        'category_id'=>$faker->numberBetween(1,5),
        'url'=>$faker->text(10),
        'image'=>$faker->text(5),
        'snippets'=>$faker->text(100)
    ];
});
